package com.example.demo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activity;

    public MonitoredData() {

    }

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public List<String> parseData(String filename){
        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            list = stream
                    .flatMap(line -> Stream.of(line.split("\t\t")))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }


    public List<MonitoredData> createMonitoredDataList(List<String> parsedData){
        List<MonitoredData> monitoredDatas = new ArrayList<>();

        int count = 0;

        for(String string: parsedData) {
            if(count == 0) {
                System.out.println("Start: " + string);
                this.setStartTime(string);
                count++;
            }

            else if(count == 1) {
                System.out.println("End: " + string);
                this.setEndTime(string);
                count++;
            }

            else if(count == 2) {
                System.out.println("Activity: " + string);
                monitoredDatas.add(new MonitoredData(this.getStartTime(), this.getEndTime(), string));
                count = 0;
            }
        }
        return monitoredDatas;
    }

    public JSONObject createJSONObjectFromMonitoredData(MonitoredData monitoredData) throws JSONException {
        JSONObject monitoredDataJson = new JSONObject();
        monitoredDataJson.put("patientId", "c3504ab4-0c0b-44d4-8808-8af6e8a840de");
        monitoredDataJson.put("startActivity", monitoredData.getStartTime());
        monitoredDataJson.put("endActivity", monitoredData.getEndTime());
        monitoredDataJson.put("activity", monitoredData.getActivity().replace("\t", ""));
        System.out.println("Sent: " + monitoredData.getStartTime() + " " + monitoredData.getEndTime() + " " + monitoredData.getActivity());
        return monitoredDataJson;
    }

}
