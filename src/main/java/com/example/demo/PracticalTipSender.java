package com.example.demo;

import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PracticalTipSender {

    private final RabbitTemplate rabbitTemplate;
    private final static Logger log = LoggerFactory.getLogger(PracticalTipSender.class);
    private static final String FILE_NAME = "activity.txt";


    public PracticalTipSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 3000L)
    public void sendPracticalTip() {
        MonitoredData data = new MonitoredData();
        List<String> parse = data.parseData(FILE_NAME);
        List<MonitoredData> monitoredDatas;
        monitoredDatas = data.createMonitoredDataList(parse);

        JSONObject dataAsJson = null;
        try {
            for(MonitoredData monitoredData: monitoredDatas){
                dataAsJson = data.createJSONObjectFromMonitoredData(monitoredData);
                rabbitTemplate.convertAndSend(DemoApplication.FANOUT_NAME, DemoApplication.ROUTING_KEY, dataAsJson.toString(1).getBytes());

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException _ignored) {
                    Thread.currentThread().interrupt();
                }
            }
        } catch (JSONException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
