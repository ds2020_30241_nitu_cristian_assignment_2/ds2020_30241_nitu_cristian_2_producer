package com.example.demo;

import org.springframework.amqp.core.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoApplication {

	public static final String EXCHANGE_NAME = "tips_tx";
	public static final String DEFAULT_PARSING_QUEUE = "default_parser_q3";
	public static final String DEFAULT_PARSING_QUEUE2 = "default_parser_q4";
	public static final String ROUTING_KEY = "tips";
	public static final String FANOUT_NAME = "tut.fanout";

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public FanoutExchange fanout() {
		return new FanoutExchange(FANOUT_NAME);
	}

	@Bean
	public Queue defaultParsingQueue() {
		return new Queue(DEFAULT_PARSING_QUEUE);
	}

	@Bean
	public Queue defaultParsingQueue2() {
		return new Queue(DEFAULT_PARSING_QUEUE2);
	}

	@Bean
	public Binding queueToExchangeBinding() {
		return BindingBuilder.bind(defaultParsingQueue()).to(fanout());
	}

	@Bean
	public Binding queueToExchangeBinding2() {
		return BindingBuilder.bind(defaultParsingQueue2()).to(fanout());
	}
}
